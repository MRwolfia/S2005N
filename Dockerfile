FROM fusuf/whatsasena:latest

RUN git clone https://github.com/MRwolfia/S2005N /root/S2005N
WORKDIR /root/S2005N/
ENV TZ=Europe/Istanbul
RUN npm install supervisor -g
RUN yarn install --no-audit

CMD ["node", "bot.js"]
